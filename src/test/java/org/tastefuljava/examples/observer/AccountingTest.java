package org.tastefuljava.examples.observer;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;
import org.h2.tools.RunScript;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import org.tastefuljava.jedo.Session;
import org.tastefuljava.jedo.SessionFactory;
import org.tastefuljava.jedo.conf.SessionFactoryBuilder;
import org.tastefuljava.jedo.conf.SessionImpl;

public class AccountingTest {
    private static final Logger LOG
            = Logger.getLogger(Accounting.class.getName());

    private static final SessionFactory factory
            = SessionFactoryBuilder.loadFrom("jedo-conf.xml").build();

    private Accounting accounting;
    private AccountObserver observer;

    @BeforeAll
    public static void initDB() throws IOException, SQLException {
        runScript("initdb.sql");
    }

    @BeforeEach
    public void init() {
        accounting = new Accounting(factory);
        observer = mock(AccountObserver.class);
        accounting.addAccountObserver(observer);
    }

    @Test
    public void testCreate() {
        accounting.createAccount("ACCOUNT1", 1000);
        accounting.createAccount("ACCOUNT2", 1000);
        accounting.transfer(100, "ACCOUNT1", "ACCOUNT2");
        assertEquals(900, accounting.getBalance("ACCOUNT1"));
        assertEquals(1100, accounting.getBalance("ACCOUNT2"));
        verify(observer).accountCreated("ACCOUNT1", 1000);
        verify(observer).accountCreated("ACCOUNT2", 1000);
        verify(observer).balanceChanged("ACCOUNT1", 900.00);
        verify(observer).balanceChanged("ACCOUNT2", 1100.00);
    }

    private static void runScript(String name)
            throws IOException, SQLException {
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        try (InputStream stream = cl.getResourceAsStream(name);
                Reader in = new InputStreamReader(stream, "UTF-8");
                Session session = factory.openSession()) {
            Connection cnt = ((SessionImpl)session).getConnection();
            RunScript.execute(cnt, in);
        }
    }
}
