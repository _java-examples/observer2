package org.tastefuljava.examples.observer;

public interface AccountObserver {
    public void accountCreated(String name, double balance);
    public void accountDeleted(String name);
    public void balanceChanged(String name, double balance);
}
