package org.tastefuljava.examples.observer;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ObserverList {
    private static final Logger LOG
            = Logger.getLogger(ObserverList.class.getName());

    private final List<Object> observers = new ArrayList<>();
    private TransactionImpl currentTrans;
 
    public void addObserver(Object observer) {
        observers.add(observer);
    }

    public void removeObserver(Object observer) {
        observers.remove(observer);
    }

    public <T> T getNotifier(Class<T> intf) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Object proxy = Proxy.newProxyInstance(loader, new Class<?>[] {intf},
                (pxy, method, args) -> {
            return dispatch(intf, method, args);
        });
        return intf.cast(proxy);
    }

    public Transaction startTransaction() {
        if (currentTrans != null) {
            throw new IllegalStateException("Already in a transaction.");
        }
        return currentTrans = new TransactionImpl();
    }

    private <T> Object dispatch(Class<T> intf, Method method, Object[] args) {
        Runnable disp = () -> {
            for (Object observer: observers) {
                if (intf.isInstance(observer)) {
                    try {
                        method.invoke(observer, args);
                    } catch (IllegalAccessException
                            | InvocationTargetException ex) {
                        LOG.log(Level.SEVERE, null, ex);
                    }
                }
            }
        };
        if (currentTrans != null) {
            currentTrans.add(disp);
        } else {
            disp.run();
        }
        return null;
    }

    private class TransactionImpl implements Transaction {
        private final List<Runnable> notificationQueue = new ArrayList<>();

        @Override
        public void commit() {
            while (!notificationQueue.isEmpty()) {
                notificationQueue.remove(0).run();
            }
        }

        @Override
        public void close() {
            notificationQueue.clear();
            currentTrans = null;
        }

        private void add(Runnable op) {
            notificationQueue.add(op);
        }
    }
}
