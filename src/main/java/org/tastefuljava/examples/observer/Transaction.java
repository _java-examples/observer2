package org.tastefuljava.examples.observer;

public interface Transaction extends AutoCloseable {
    @Override
    public void close();
    public void commit();
}
