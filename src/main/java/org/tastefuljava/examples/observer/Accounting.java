package org.tastefuljava.examples.observer;

import org.tastefuljava.jedo.Session;
import org.tastefuljava.jedo.SessionFactory;

public class Accounting {
    private final SessionFactory sessionFactory;
    private final ObserverList observers = new ObserverList();
    private final AccountObserver notifier
            = observers.getNotifier(AccountObserver.class);

    public Accounting(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void addAccountObserver(AccountObserver obs) {
        observers.addObserver(obs);
    }

    public void removeAccountObserver(AccountObserver obs) {
        observers.removeObserver(obs);
    }

    public void createAccount(String name, double initialBalance) {
        try (Transaction tr = observers.startTransaction();
                Session session = sessionFactory.openSession()) {
            Account account = new Account();
            account.setName(name);
            account.setBalance(initialBalance);
            session.insert(account);
            notifier.accountCreated(name, initialBalance);
            session.commit();
            tr.commit();
        }
    }

    public void deleteAccount(String name) {
        try (Transaction tr = observers.startTransaction();
                Session session = sessionFactory.openSession()) {
            Account account = session.load(Account.class, name);
            if (account == null) {
                throw new IllegalArgumentException("Account not found: " + name);
            }
            session.delete(account);
            notifier.accountDeleted(name);
            session.commit();
        }
    }

    public double getBalance(String name) {
        try (Session session = sessionFactory.openSession()) {
            Account account = session.load(Account.class, name);
            if (account == null) {
                throw new IllegalArgumentException("Account not found: " + name);
            }
            return account.getBalance();
        }
    }

    public void transfer(double amount, String from, String to) {
        try (Transaction tr = observers.startTransaction();
                Session session = sessionFactory.openSession()) {
            Account fromAccount = session.load(Account.class, from);
            if (fromAccount == null) {
                throw new IllegalArgumentException("Account not found: " + from);
            }
            fromAccount.debit(amount);
            session.update(fromAccount);
            notifier.balanceChanged(
                    fromAccount.getName(), fromAccount.getBalance());
            Account toAccount = session.load(Account.class, to);
            if (fromAccount == null) {
                throw new IllegalArgumentException("Account not found: " + to);
            }
            toAccount.credit(amount);
            session.update(toAccount);
            notifier.balanceChanged(
                    toAccount.getName(), toAccount.getBalance());
            session.commit();
            tr.commit();
        }
    }
}
